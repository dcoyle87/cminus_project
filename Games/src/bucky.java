import java.awt.DisplayMode;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;



public class bucky {

	public static void main(String[] args) {
		
		DisplayMode dm = new DisplayMode(800, 600, 16, DisplayMode.REFRESH_RATE_UNKNOWN);
		bucky b = new bucky();
		b.run();
	}
	
	private Animation a;
	private Animation b;
	private ScreenManager s;
	private Image bg;
	private static final DisplayMode modes1[] = {
		new DisplayMode(800, 600, 32, 0),
		new DisplayMode(800, 600, 24, 0),
		new DisplayMode(800, 600, 16, 0),
		new DisplayMode(640, 480, 32, 0),
		new DisplayMode(640, 480, 24, 0),
		new DisplayMode(640, 480, 16, 0),
	};
	
	//load images and add scenes
	public void loadImages(){
		bg = new ImageIcon("/Users/dcoyle87/programming/Games/back.JPEG").getImage();
		Image face1 = new ImageIcon("/Users/dcoyle87/programming/Games/face.PNG").getImage();
		Image face2 = new ImageIcon("/Users/dcoyle87/programming/Games/face2.PNG").getImage();
		Image Bar1 = new ImageIcon("/Users/dcoyle87/programming/Games/Bar1.PNG").getImage();
		Image Bar2 = new ImageIcon("/Users/dcoyle87/programming/Games/Bar2.PNG").getImage();
		
		a = new Animation();
		a.addScene(face1, 250);
		a.addScene(face2, 250);
		a.addScene(Bar1,250);
		a.addScene(Bar2, 250);
		
		b = new Animation();
		b.addScene(face1, 250);
		b.addScene(face2, 250);
		b.addScene(Bar1,250);
		b.addScene(Bar2, 250);
	}
	
	//main method called from main
	public void run(){
		s = new ScreenManager();
		try{
			DisplayMode dm = s.findFirstCompatibleMode(modes1);
			s.setFullScreen(dm);
			loadImages();
			movieLoop();
		}finally{
			s.restoreScreen();
		}
	}
	
	//play movie
	public void movieLoop(){
		long startingTime = System.currentTimeMillis();
		long cumTime = startingTime;
		
		while(cumTime - startingTime<10000){
			long timePassed = System.currentTimeMillis() -cumTime;
			cumTime += timePassed;
			a.update(timePassed);
			b.update(timePassed);
			
			//draw and update screen
			
			Graphics2D g = s.getGraphics();
			draw(g);
			g.dispose();
			s.update();
			try{
				Thread.sleep(20);	
			}catch (Exception e){}
		}
	}
	
	//draws graphics
		public void draw(Graphics g){
			g.drawImage(bg, 0, 0, null);
			g.drawImage(a.getImage(), 0, 0, null);
			g.drawImage(b.getImage(), 100, 100, null);
			
		}

}
