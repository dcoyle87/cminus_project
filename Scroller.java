import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Scroller extends JPanel implements Runnable{
  JLabel label;
  String str = "    ";

  public Scroller(){
    super();
    label = new JLabel(str);
    label.setForeground(Color.WHITE);
    label.setBounds(100, 30, 300, 50);
    add(label);
    Thread t = new Thread(this);
    t.start();
  }

  public void run(){
    while(true){
        char c = str.charAt(0);
        String rest = str.substring(1);
        str = rest + c;
        label.setText(str);
        try{
            Thread.sleep(150);
        }catch(InterruptedException e){}
    }
  }
  
  public void getArtistInfo(String song, String artist, String album){
	  str = " [Song]: " +song+ " [Artist]: " +artist+ " [Album]: " +album;
  }
  
 }
