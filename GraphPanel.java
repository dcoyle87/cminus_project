import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;


public class GraphPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3877127566250754129L;
	private final Image image;

    public GraphPanel(Image image) {
        this.image = image;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(this.image, 0, 0, getWidth(), getHeight(), this);
    }

}
