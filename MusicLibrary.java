import java.io.File;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


public class MusicLibrary {
	
		// model for displaying music library
		DefaultTableModel model = new DefaultTableModel();
		File[] trackList = new File[100];
		int i;
		
		public TableModel getModel(){
			return model;
		}
		
		public void importSongToLibrary(File file, String song, String time, String artist, String album){
			model.addRow(new Object[]{song, time, artist, album});
			trackList[i] = file;
			i++;
		}
		
		public File getTrack(int i){
			return trackList[i];
		}
		
		public int getTrackIndex(int i){
			return i;
		}
		
		public File getNextTrack(int i){
			if(trackList[i+1]!=null){
				return trackList[i+1];
			}
			return trackList[i];
		}
		
		public File getPrevTrack(int i){
			if(trackList[i-1]!=null){
				return trackList[i-1];
			}
			return trackList[i];
		}
		
		public void addHeaders(){
			model.addColumn("Song");
			model.addColumn("Time");
			model.addColumn("Artist");
			model.addColumn("Album");
		}
}
