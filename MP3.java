import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.EventListener;

import javax.swing.BoundedRangeModel;
import javax.swing.BoxLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JToggleButton;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class MP3 {

	public static void main(String[] args) {

		
		Runnable runner = new Runnable() {
			
			int row = 0;
			int diffSong = 0;
			
			@Override
			public void run() {
				
				
				final JFrame frame = new JFrame("C-- Music Player");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
				final Container contentPane = new Container();
		
				final Image equalizer = new ImageIcon("equalizer.gif").getImage();
				final Image equalizerOff = new ImageIcon("equalizer_off.PNG").getImage();
				
				contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
				
				// creating menuBar
				JMenuBar menuBar = new JMenuBar();
				JMenu fileMenu = new JMenu("File");
				menuBar.add(fileMenu);
				JMenuItem importItem = new JMenuItem("Import");
				fileMenu.add(importItem);
					
				// create instance of scroller
				final Scroller buttonPanel = new Scroller();
				buttonPanel.setLayout(null);
				buttonPanel.setBackground(Color.black);
				
				// create instance of music library
				final MusicLibrary musicLibrary = new MusicLibrary();
				musicLibrary.addHeaders();
				
				// JTable to store music library
				final JTable musicTable = new JTable(musicLibrary.getModel());
				
				// Scroll pane to store JTable
				final JScrollPane songScrollPane = new JScrollPane(musicTable);
				songScrollPane.setBorder(new SoftBevelBorder(BevelBorder.LOWERED));
				songScrollPane.setBackground(Color.lightGray);
				songScrollPane.setPreferredSize(new Dimension(10, 10));
				
				
				final GraphPanel graphics = new GraphPanel(equalizer);
				final GraphPanel off = new GraphPanel(equalizerOff);
				graphics.setVisible(true);
			
					
				contentPane.add(buttonPanel);
				contentPane.add(off);
					
				
				frame.add(contentPane);
					
					
				// Icons
				final Icon play_icon = new ImageIcon("play_icon.png");
				final Icon pause_icon = new ImageIcon("pause_icon.png");
				Icon lo_vol_icon = new ImageIcon("vol_low_icon.png");
				Icon hi_vol_icon = new ImageIcon("vol_hi_icon.png");
				Icon reverse_icon = new ImageIcon("back_icon.png");
				Icon forward_icon = new ImageIcon("forward_icon.png");
				
				// Play/Pause button
				final JToggleButton play = new JToggleButton(play_icon);
				play.setBounds(235, 100, 32, 32);
				
				// Volume Icons
				JLabel loVolume = new JLabel(lo_vol_icon);
				loVolume.setBounds(145, 153, 16, 16);
				JLabel hiVolume = new JLabel(hi_vol_icon);
				hiVolume.setBounds(340, 153, 16, 16);

				
				//  Check box to toggle between song lists and equalizer
				final JCheckBox songList = new JCheckBox("Library");
				songList.setForeground(Color.WHITE);
				songList.setBackground(Color.BLACK);
				songList.setBounds(398, 170, 100, 100);
				
				// Creating Shuffle check box
				JCheckBox shuffle = new JCheckBox("shuffle");
				shuffle.setForeground(Color.WHITE);
				shuffle.setBackground(Color.BLACK);
				shuffle.setBounds(2, 170, 100, 100);
				
				// Creating reverse and forward Buttons
				JButton reverse = new JButton(reverse_icon);
				reverse.setBounds(190, 100, 32, 32);
				JButton forward = new JButton(forward_icon);
				forward.setBounds(280, 100, 32, 32);
				
				// Creating Volume slider
				JSlider jslider = new JSlider();
				jslider.setBounds(160, 150, 180, 25);
				jslider.setBackground(Color.BLACK);
				
				// Adding Buttons to button Panel
				buttonPanel.add(play);
				buttonPanel.add(reverse);
				buttonPanel.add(forward); 
				buttonPanel.add(jslider);
				buttonPanel.add(shuffle);
				buttonPanel.add(loVolume);
				buttonPanel.add(hiVolume);
				buttonPanel.add(songList);
				
				// create instance of JMFPlayer
				final JMFPlayer player = new JMFPlayer();
				
				
				//itemListener for toggle button, play and pause song
				ItemListener playListener = new ItemListener(){
					public void itemStateChanged(ItemEvent itemEvent){
						
						boolean libChange = songList.isSelected();
						
						int state = itemEvent.getStateChange();
						if(libChange == false){
							if(state == 1){
								contentPane.remove(off);
								contentPane.revalidate();
								contentPane.add(graphics);
								contentPane.revalidate();	
							} else {
								contentPane.remove(graphics);
								contentPane.revalidate();
								contentPane.add(off);
								contentPane.revalidate();
							}
						}
						
						if(state == 1){
							play.setIcon(pause_icon);
							if(diffSong == 1)player.loadSong(musicLibrary.getTrack(row));
							player.play();
							play.updateUI();
							diffSong = 0;
							buttonPanel.getArtistInfo(player.getSongName(), player.getArtistName(), player.getAlbumName());
						} else {
							play.setIcon(play_icon);
							player.pause();
							play.updateUI();
						}
						
					}
				};
				
				
				//itemListener for library checkbox, switch graphics/table
				ItemListener library = new ItemListener() {	
					@Override
					public void itemStateChanged(ItemEvent e) {
						
						boolean libChange = songList.isSelected();
						boolean isPlaying = play.isSelected();
						
						if(libChange==true){
							contentPane.remove(graphics);
							contentPane.remove(off);
							contentPane.revalidate();
							contentPane.add(songScrollPane);
							contentPane.revalidate();
						}else{
							contentPane.remove(songScrollPane);
							contentPane.revalidate();
							if(isPlaying == true){
								contentPane.add(graphics);
								contentPane.revalidate();
							} else {
								contentPane.add(off);
								contentPane.revalidate();
							}
						}
					}
				};
			
				
				//actionListener for importing songs
				ActionListener importListener = new ActionListener(){
					public void actionPerformed(ActionEvent event){
						JFileChooser fileChooser = new JFileChooser();
						JMFPlayer player = new JMFPlayer();
						int status = fileChooser.showOpenDialog(contentPane);
						if(status == JFileChooser.APPROVE_OPTION){
							player.loadSong(fileChooser.getSelectedFile());
							musicLibrary.importSongToLibrary(fileChooser.getSelectedFile(), player.getSongName(),
										player.getSongDuration(), player.getArtistName(), player.getAlbumName());
						}
					}
				};
				
				
				//actionListener for Next song button
				ActionListener nextListener = new ActionListener(){
					public void actionPerformed(ActionEvent event){
						if((player.isPlaying()!=500)&&(musicLibrary.getNextTrack(row)!=null))
						{
							player.pause();
							player.loadSong(musicLibrary.getNextTrack(row));
							musicTable.changeSelection(row+1, 0, false, false);
							player.play();
							row++;
						}
						else{
							//do nothing
						}
					}
				};
				
				
				//actionListener for Previous song button
				ActionListener prevListener = new ActionListener(){
					public void actionPerformed(ActionEvent event){
						if((player.isPlaying()!=500)&&(musicLibrary.getTrackIndex(row)!=0))
						{
							player.pause();
							player.loadSong(musicLibrary.getPrevTrack(row));
							musicTable.changeSelection(row-1, 0, false, false);
							player.play();
							row--;
						}
						else{
							//do nothing
						}
					}
				};	
				
				
				// Listener for table selection
				MouseListener mouseTableSelector = new MouseAdapter(){
					
					@Override
					public void mouseClicked(MouseEvent event) {
						row = musicTable.rowAtPoint(event.getPoint());
						player.selectSongInTable(musicLibrary.getTrack(row));
						diffSong = 1;
					}
					
					@Override
					public void mousePressed(MouseEvent e) {
						// TODO Auto-generated method stub
					}

					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub
					}

					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub
					}

					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub	
					}
				};

				
				//listener for JSlider, adjust volume
				class BoundedChangeListener implements ChangeListener {
					public void stateChanged(ChangeEvent changeEvent){
						Object source = changeEvent.getSource();
						if(source instanceof BoundedRangeModel){
							BoundedRangeModel aModel = (BoundedRangeModel) source;
							if(!aModel.getValueIsAdjusting()){
								System.out.println("Changed: " + aModel.getValue());
							}
						} else if (source instanceof JSlider){
							JSlider theJSlider = (JSlider) source;
							if(!theJSlider.getValueIsAdjusting()){
							int volumeInt = theJSlider.getValue();
							float volumeFloat = new Float(volumeInt);
							volumeFloat = volumeFloat/100;
							if(volumeFloat > 0.8) volumeFloat = (float) 0.8;
							player.setGain(volumeFloat);
						}
					} else {
						System.out.println("Someting changed: " +source);
						}
					}
				}
				
				musicTable. addMouseListener(mouseTableSelector);
				importItem.addActionListener(importListener);
				play.addItemListener(playListener);
				jslider.addChangeListener(new BoundedChangeListener());
				songList.addItemListener(library);
				forward.addActionListener(nextListener);
				reverse.addActionListener(prevListener);
				
				frame.setJMenuBar(menuBar);
				frame.setSize(500, 530);
				frame.setVisible(true);
				
			}
};
	EventQueue.invokeLater(runner);
	}
}