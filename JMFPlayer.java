import java.awt.Component;
import java.io.*;
import java.net.MalformedURLException;
import java.text.DecimalFormat;

import javax.media.*;


public class JMFPlayer{

	private Player player;
	
	File mediaFile = null;
	
	@SuppressWarnings("deprecation")
	public JMFPlayer(){
		if(mediaFile != null){
		try {
			player = Manager.createRealizedPlayer(mediaFile.toURL());
		} catch (NoPlayerException | IOException | CannotRealizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}
	
	public void selectSongInTable(File file){
		mediaFile = new File(file.toString());
	}
	
	public void loadSong(File file){
		mediaFile = new File(file.toString());
		try {
			player = Manager.createRealizedPlayer(mediaFile.toURL());
		} catch (NoPlayerException | CannotRealizeException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void play(){
		player.start();
	}
	
	public void pause(){
		player.stop();
	}
	
	public void setGain(float f){
		player.getGainControl().setLevel(f);
	}
	
	public int isPlaying(){
		return player.getState();
	}
	
	public String getSongDuration(){
		Time time = player.getDuration();
		DecimalFormat df1 = new DecimalFormat("0");
		DecimalFormat df2 = new DecimalFormat("00");
		double songTime = time.getSeconds();
		double minutes = Math.floor((double) (songTime/60));
		double seconds = ((songTime/60) - Math.floor(songTime/60))*60;
		String minutesString = df1.format(minutes);
		String secondsString = df2.format(seconds);
		String totalTime = minutesString + ":" + secondsString;
		return totalTime;
	}
	
	public String getSongName(){
		String[] fileName = mediaFile.getName().split("=");
		return fileName[0];
	}
	
	public String getArtistName(){
		String[] fileName = mediaFile.getName().split("=");
		return fileName[1];
	}
	
	public String getAlbumName(){
		String[] fileName = mediaFile.getName().split("=");
		String[] albumName = fileName[2].split("\\.wav");
		return albumName[0];
	}

}
	

